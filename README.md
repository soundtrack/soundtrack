### SoundTrack Main Application

This is the main part of SoundTrack project. Only one file: `docker-compose.yaml`. So please make sure that `Docker` and `Compose` are installed on your machine.

```shell
$ docker version
$ docker-compose version
```

Let's suppose you have three beacons, you have to write them positions into `beacon.json`, i.e :

```json
[
  {
    "id": "beacon_1",
  	"Position": {
  		"X": 5,
  		"Y": 6
  	}
  },
  {
    "id": "beacon_2",
  	"Position": {
  		"X": 1,
  		"Y": 2
  	}
  },
  {
    "id": "beacon_3",
  	"Position": {
  		"X": 2,
  		"Y": 3
  	}
  },
]
```


Then, `docker-compose.yaml` file will pull three images: `mongo`, `middleware` and `webapp` directly from `registry.gitlab.com` or `hub.docker.com`. So you need to be logged into `registry.gitlab.com`

```shell
$ docker login registry.gitlab.com
```

Then, you're ready to start `SoundTrack` application: ! 

Just run,

```shell
$ docker-compose up -d
```

Then go on [localhost](http://localhost:8080) for getting the `Web Application`. :smiley:

Also, there is a server for handling beacon connections. Find your IP address with:

```shell
$ ifconfig # On Linux
$ ipconfig # On Windows
```

You can connect your `beacon` to `ip@12800` and send `values.json` data.